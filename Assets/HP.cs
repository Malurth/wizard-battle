﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HP : MonoBehaviour{

	public float maxHP = 100;
	public float curHP = 100;
	public Slider HPslider;
	public int pointsToWin = 10;

	// Use this for initialization
	void Start () {
		curHP = maxHP;
		HPslider = GameObject.Find("HPSlider").GetComponent<Slider>();
	}
	
	[RPC]
	public void TakeDamage(float amt, PhotonPlayer source)
	{
		if (!PhotonNetwork.player.Equals(source)) {
			curHP = curHP - amt;
			if (GetComponent<PhotonView> ().isMine) {
				if (gameObject.tag == "Player") {
					HPslider.value = curHP;
				}
			}

			if (curHP <= 0) {
				Debug.Log ("DYING---------------------------------------");
				//HPslider.value = 0;
				Die ();
				source.AddScore (1);
				checkVictory (source);
			}
		}
	}

	void checkVictory(PhotonPlayer who) {
		if (who.GetScore() >= pointsToWin) {
			NetworkManager NMScript = GetComponent<NetworkManager>();
			NMScript.EndGame(who);
		}
	}

	[RPC]
	public void Die() {
		if (GetComponent<PhotonView>().instantiationId == 0) {
			Destroy(gameObject);
		} else {
			if (GetComponent<PhotonView>().isMine) {

				if( gameObject.tag=="Player"){	// player object
					NetworkManager nm = GameObject.FindObjectOfType<NetworkManager>();

					nm.standbyCamera.SetActive(true);
					nm.respawnTimer = 3f;
					nm.alive = false;

				}

				PhotonNetwork.Destroy(gameObject);
			}
		}
	}
}












