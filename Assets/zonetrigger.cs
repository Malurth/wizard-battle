﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class zonetrigger : MonoBehaviour {
	public int captureThreshold;
	public int curCapturePoints = 0;
	public bool captured = false;
	Slider slider;
	public Image fill;

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Player") {
			Debug.Log ("Player entered " + this.name);
			slider = GameObject.Find("CaptureSlider").GetComponent<Slider>();
			if (!captured) {
				slider.GetComponent<CanvasGroup>().alpha = 1;
			}
			if (this.name == "Zone 1") {
				fill.color = Color.red;
			} else if (this.name == "Zone 2") {
				fill.color = Color.blue;
			} else if (this.name == "Zone 3") {
				fill.color = Color.yellow;
			} else if (this.name == "Zone 4") {
				fill.color = Color.magenta;
			} 
		}
	}

	void OnTriggerStay(Collider other) {
		if (other.gameObject.tag == "Player") {
			Debug.Log ("Player staying in " + this.name);
			if (curCapturePoints < captureThreshold) {
				curCapturePoints++;
				slider.value = curCapturePoints;
				if (curCapturePoints >= captureThreshold) {
					captured = true;
					CastSpell castspell = other.gameObject.GetComponent<CastSpell>();
					castspell.Powerup();
					Debug.Log (this.name + " captured.");
					slider.GetComponent<CanvasGroup>().alpha = 0;
					slider.value = 0;
				}
			}
		}
	}

	void OnTriggerExit(Collider other) {
		if (other.gameObject.tag == "Player" && !captured) {
			curCapturePoints = 0;
			Debug.Log ("Capture points cleared.");
			slider.GetComponent<CanvasGroup>().alpha = 0;
			slider.value = 0;
		}
	}
}
