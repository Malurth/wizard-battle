﻿using UnityEngine;
using System.Collections;

public class NetworkSpell : Photon.MonoBehaviour {

	Vector3 realPosition = Vector3.zero;
	Quaternion realRotation = Quaternion.identity;
	Transform caster;
	public float damage = 25;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (photonView.isMine) {
			//do nothing
		} else {
			transform.position = Vector3.Lerp(transform.position, realPosition, 0.12f);
			transform.rotation = Quaternion.Lerp(transform.rotation, realRotation, 0.12f);
		}
	}
	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
		if (stream.isWriting) { //our player, send player data to network
			stream.SendNext (transform.position);
			stream.SendNext (transform.rotation);
		} else { //other players, update game state based on data recieved
			realPosition = (Vector3)stream.ReceiveNext();
			realRotation = (Quaternion)stream.ReceiveNext();
		}
	}


	void OnTriggerEnter(Collider other)
	{
		Debug.Log(other.name);
		if (other.gameObject.tag == "Player")
		{
			HP health = other.gameObject.GetComponent<HP>(); //GetComponent<HP>();
			if(!health.GetComponent<PhotonView>().isMine) { 
				health.GetComponent<PhotonView>().RPC("TakeDamage", PhotonTargets.All, damage, PhotonNetwork.player);
			}

			//health.TakeDamage(25);
			PhotonNetwork.Destroy(gameObject);
		}
	}

}
