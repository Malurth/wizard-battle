﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NetworkManager : Photon.MonoBehaviour {

	public GameObject standbyCamera;
	public GameObject HUD;
	public GameObject WinHUD;
	public GameObject LoseHUD;
	public float gameTimeSeconds;
	public float respawnTimer = 0;
	public float newGameTimer = 0;
	public bool alive;
	public bool timerRunning = false;
	public GameObject _myPlayer;
	private ExitGames.Client.Photon.Hashtable roomProps = new ExitGames.Client.Photon.Hashtable();
	SpawnPoint[] spawns;

	// Use this for initialization
	void Start () {
		Screen.showCursor = false;
		Screen.lockCursor = true;
		Connect ();
	}

	void Update() {
		if (timerRunning && PhotonNetwork.isMasterClient) {
			roomProps = PhotonNetwork.room.customProperties;
			roomProps["Timer"] = (float)roomProps["Timer"] - Time.deltaTime;
			PhotonNetwork.room.SetCustomProperties(roomProps);
		}

		if (timerRunning && (float)PhotonNetwork.room.customProperties["Timer"] <= 0) {
			//this.GetComponent<PhotonView>().RPC("DecideWinner", PhotonTargets.All);
			DecideWinner();
		}

		if (respawnTimer > 0) {
			respawnTimer -= Time.deltaTime;
			if(respawnTimer <= 0){
				SpawnMe();
			}	
		}

		if (PhotonNetwork.isMasterClient && newGameTimer > 0) {
			newGameTimer -= Time.deltaTime;
			if(newGameTimer <= 0){
				photonView.RPC("NewGame", PhotonTargets.All);
				//NewGame();
			}	
		}

	}

	[RPC]
	public void DecideWinner() { //technically I think this player list refers to everyone regardless of room, but there's prolly only ever gonna be one room anyway
		Debug.Log ("Deciding Winner");
		timerRunning = false;
		respawnTimer = 0;
		GameObject[] spells = GameObject.FindGameObjectsWithTag("Spell");
		foreach (GameObject spell in spells) {
			Destroy (spell);
		}
		if (alive) {
			GameObject.Find("CaptureSlider").GetComponent<Slider>().GetComponent<CanvasGroup>().alpha = 0;
			TogglePlayerControls (false);
		}
		int threshold = -1;
		PhotonPlayer winner = null;
		foreach (PhotonPlayer p in PhotonNetwork.playerList) {
			if (p.GetScore() > threshold) {
				threshold = p.GetScore();
				winner = p;
			}
		}
		if (winner != null) {
			EndGame(winner);
		}
	}

	public void EndGame(PhotonPlayer winner) {
		Debug.Log ("Ending game, winner is ID " + winner.ID);
		HUD.SetActive (false);
		if (winner == PhotonNetwork.player) {
			WinHUD.SetActive (true);
		} else {
			LoseHUD.SetActive (true);
		}
		if (PhotonNetwork.isMasterClient) {
			newGameTimer = 7f;
		}
	}

	[RPC]
	void NewGame() {
		Debug.Log ("Starting new game.");
		//every alive player needs to die first
		if (alive) {
			HP health = _myPlayer.GetComponent<HP>();
			health.Die();
			respawnTimer = 0;
		}
		if (PhotonNetwork.isMasterClient) {
			roomProps["Timer"] = gameTimeSeconds;
			foreach (PhotonPlayer p in PhotonNetwork.playerList) {
				p.SetScore (0);
			}
		}
		SpawnMe ();
		timerRunning = true;
	}

	void Connect() {
		PhotonNetwork.ConnectUsingSettings ("Alpha2"); //string is version, must match between players
	}

	void OnGUI() {
		if (PhotonNetwork.connectionStateDetailed.ToString () != "Joined") {
			GUILayout.Label (PhotonNetwork.connectionStateDetailed.ToString ());
		}
	}

	void OnJoinedLobby() { //connected to photon network
		if (PhotonNetwork.countOfRooms < 1) {
			PhotonNetwork.CreateRoom(null); //also joins it
			Debug.Log("Created room");
		} else {
			PhotonNetwork.JoinRandomRoom();
		}
	}

	void OnCreatedRoom() {
		roomProps.Add("Timer", gameTimeSeconds);
		PhotonNetwork.room.SetCustomProperties(roomProps);
		timerRunning = true;
	}

	void OnJoinedRoom() {
		Debug.Log("Joined room");
		timerRunning = true;
		PhotonNetwork.player.SetScore(0);
		spawns = GameObject.FindObjectsOfType<SpawnPoint> ();
		SpawnMe();
	}

	void TogglePlayerControls(bool wat) {
		((MonoBehaviour)_myPlayer.GetComponent("FPSInputController")).enabled = wat;
		((MonoBehaviour)_myPlayer.GetComponent("MouseLook")).enabled = wat;
		((MonoBehaviour)_myPlayer.GetComponent("CastSpell")).enabled = wat;
		((MonoBehaviour)_myPlayer.GetComponent("CharacterMotor")).enabled = wat;
		((MonoBehaviour)_myPlayer.transform.FindChild ("Main Camera").gameObject.GetComponent("MouseLook")).enabled = wat;
	}

	void SpawnMe() {
		Debug.Log ("Spawning.");
		alive = true;
		if (spawns.Length < 1) {
			Debug.Log ("No spawn points detected, aborting.");
			return;
		}
		GameObject[] zones = GameObject.FindGameObjectsWithTag("Zone");
		foreach (GameObject zone in zones) {
			zone.GetComponent<zonetrigger>().curCapturePoints = 0;
			zone.GetComponent<zonetrigger>().captured = false;
		}
		SpawnPoint _spawnPoint = spawns [Random.Range (0, spawns.Length)];
		_myPlayer = PhotonNetwork.Instantiate("PlayerCharacter", _spawnPoint.transform.position, _spawnPoint.transform.rotation, 0);
		TogglePlayerControls (true);
		CastSpell castspell = _myPlayer.gameObject.GetComponent<CastSpell>();
		castspell.Depower();
		_myPlayer.transform.FindChild ("Main Camera").gameObject.SetActive (true);
		HUD.SetActive (true);
		GameObject.Find("HPSlider").GetComponent<Slider>().value = 100;
		GameObject.Find("CaptureSlider").GetComponent<Slider>().value = 0;
		GameObject.Find("CaptureSlider").GetComponent<Slider>().GetComponent<CanvasGroup>().alpha = 0;
		standbyCamera.SetActive (false);
		WinHUD.SetActive (false);
		LoseHUD.SetActive (false);
	}

}



















