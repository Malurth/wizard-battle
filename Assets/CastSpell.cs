using UnityEngine;
using System.Collections;

public class CastSpell : MonoBehaviour {
	
	public string Spell;
	public double coolDown = 0;
	public GameObject spellInstance;
	public GameObject camera;
	int spellspeed = 1000;
	double cdReset = 1;

	// Use this for initialization
	void Start () {
		camera = GameObject.Find ("Main Camera");
	}
	
	// Update is called once per frame
	void Update () {
		coolDown -= Time.deltaTime;
		if (Input.GetButton ("Fire1")) {
			if (coolDown <= 0) {
				if (camera != null) {
					Vector3 spellPos = (camera.transform.forward)*2 + transform.position; 
					spellPos.y += 2;
					spellInstance = PhotonNetwork.Instantiate (Spell, spellPos , camera.transform.rotation, 0);
					spellInstance.rigidbody.AddForce(camera.transform.forward * spellspeed);
					coolDown = cdReset;
				} else {
					camera = GameObject.Find ("Main Camera");
				}
			}
		}
	}

	public void Powerup() {
		spellspeed += 1000;
		cdReset -= .2;
	}

	public void Depower() {
		spellspeed = 1000;
		cdReset = 1;
	}





	/*void Cast () {
			if(coolDown <= 0){
				spellInstance = Instantiate (spell, Camera.main.transform.position, Camera.main.transform.forward) as Rigidbody;
				spellInstance.AddForce(Camera.main.transform.forward* 5000);
				coolDown = 1.5;
			}
	}*/
}
