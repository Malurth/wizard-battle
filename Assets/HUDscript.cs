﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUDscript : MonoBehaviour {

	public Text timertext;
	public Text scoretext;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (this.enabled) {
			float rawTimer = (float)PhotonNetwork.room.customProperties["Timer"];
			int timerMinutes = (int)(rawTimer / 60);
			int timerSeconds = (int)(rawTimer % 60);
			string extraZero;
			if (timerSeconds < 10) {
				extraZero = "0";
			} else {
				extraZero = "";
			}
			timertext.text = timerMinutes + ":" + extraZero + timerSeconds;
			scoretext.text = PhotonNetwork.player.GetScore().ToString();
		}
	}
}
