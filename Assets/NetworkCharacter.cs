using UnityEngine;
using System.Collections;

public class NetworkCharacter : Photon.MonoBehaviour {

	Vector3 realPosition = Vector3.zero;
	Quaternion realRotation = Quaternion.identity;
	float lastUpdateTime;
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (photonView.isMine) {
			//do nothing
		} else {
			transform.position = Vector3.Lerp(transform.position, realPosition, 0.12f);
			transform.rotation = Quaternion.Lerp(transform.rotation, realRotation, 0.12f);
		}
	}

	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
		if (stream.isWriting) { //our player, send player data to network
			stream.SendNext (transform.position);
			stream.SendNext (transform.rotation);
		} else { //other players, update game state based on data recieved
			realPosition = (Vector3)stream.ReceiveNext();
			realRotation = (Quaternion)stream.ReceiveNext();
		}
	}

	//void OnTriggerEnter(Collider other)
	//{
		//Debug.Log ("NET_CHAR: " + other.name);

	//}





}
